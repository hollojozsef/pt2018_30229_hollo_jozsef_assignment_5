package PT2018.Tema5;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;

public class Model {
	ArrayList<MonitoredData> data = new ArrayList<MonitoredData>();

	public void read(File file) throws IOException {
		Scanner scanner = new Scanner(file);
		org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		while (scanner.hasNext()) {
			String[] tokens = scanner.nextLine().split("		");
			DateTime start = formatter.parseDateTime(tokens[0]);
			DateTime end = formatter.parseDateTime(tokens[1]);
			String[] tokens2 = tokens[2].split("	");
			String activity = tokens2[0];
			MonitoredData mon = new MonitoredData(start, end, activity);
			System.out.println(mon);

			data.add(mon);
		}
	}

	public void getDays() {
		Long nr = data.stream().map(date -> date.getStartTime().dayOfYear()).distinct().collect(Collectors.counting());
		System.out.println(nr);
	}

	public void getActivity() {
		Map<String, Long> act = data.stream()
				.collect(Collectors.groupingBy(data -> data.getActivity(), Collectors.counting()));
		PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter("AllActivities.txt", true), true);
			out.println(act);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getActivityByDay() {
		Map<Integer, Map<String, Long>> map = data.stream()
				.collect(Collectors.groupingBy(data -> ((MonitoredData) data).getStartTime().getDayOfYear(),
						Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
		PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter("output.txt", true), true);
			out.println(map);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//
		System.out.println(map);
	}
	public void getEntireDuration() {
		Map<String, Long> map4 = new HashMap<String, Long>();

		map4 = data.stream()
		    .collect(
		        Collectors.groupingBy(
		            MonitoredData::getActivity, 
		            Collectors.summingLong(MonitoredData::getDuration)
		        )
		    );
		System.out.println(map4);
	}
	public void getActivityShort() {
		Map<String, Long> act = data.stream()
				.collect(Collectors.groupingBy(data -> data.getActivity(), Collectors.counting()));
		Map<String, Long> act2 = data.stream()
				.filter(data->{
					Interval period = new Interval(data.getStartTime(), data.getEndTime());
					if(period.toDurationMillis()<300000)
						return true;
					else
						return false;
				})
				.collect(Collectors.groupingBy(data -> data.getActivity(), Collectors.counting()));
		List<String> result=act2.entrySet().stream()
				.filter((data)->{
					if(data.getValue()/act.get(data.getKey())>0.9)
						return true;
					else
						return false;
				})
				.map((a) -> a.getKey())
				.collect(Collectors.toList());
		System.out.println(result);
		}
}
