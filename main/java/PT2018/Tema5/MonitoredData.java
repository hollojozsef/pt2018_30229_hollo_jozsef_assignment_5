package PT2018.Tema5;

import java.time.Duration;

import org.joda.time.DateTime;
import org.joda.time.Interval;

public class MonitoredData {
	public DateTime startTime;
	public DateTime endTime;
	public String activity;
	public MonitoredData(DateTime startTime, DateTime endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	public DateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}
	public DateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public long getDuration() {
		Interval period = new Interval(getStartTime(), getEndTime());
		return period.toDurationMillis();
	}
	@Override
	public String toString() {
		return "startTime=" + startTime + ", endTime=" + endTime + ", activity=" + activity ;
	}
	
}
